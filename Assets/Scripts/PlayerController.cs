﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    CharacterController2D controller;

    public bool spawnedPlayer = false;
    void Start()
    {
       
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnPlayer()
    {
        if (!spawnedPlayer) {
            Instantiate(transform.gameObject, transform.position, transform.rotation);
            spawnedPlayer = true;
        }
            
    }


    private void FixedUpdate()
    {
        
    }
}
